module pyriand3r/astrid

go 1.14

require (
	github.com/apex/log v1.1.2
	github.com/dsoprea/go-exif/v2 v2.0.0-20200321225314-640175a69fe4
	github.com/dsoprea/go-jpeg-image-structure v0.0.0-20200322161747-68b42ab8823c
	github.com/pkg/errors v0.8.1
)
