package info_extractor

import (
	"bytes"
	"github.com/dsoprea/go-exif/v2"
	jpegstructure "github.com/dsoprea/go-jpeg-image-structure"
	"github.com/pkg/errors"
	"path/filepath"
	"regexp"
	"time"
)

type ImageInfo struct {
	Date time.Time
	Ext string
}

// ExtractImageInfo extracts all possible data from the image (exif if possible)
// and returns it in an ImageInfo struct
func ExtractImageInfo(path string) (ImageInfo, error) {
	var info ImageInfo
	if err := fillWithExif(path, &info); err != nil {
		if err2 := fillWithFileNameData(path, &info); err2 != nil {
			return info, err2
		}
	}

	info.Ext = filepath.Ext(path)

	return info, nil
}

// getNameFromExif extracts data from exif
func fillWithExif(path string, info *ImageInfo) error {
	ifdIb, err := getExif(path)
	if err != nil {
		return err
	}

	// extract creation date
	tag, err := ifdIb.FindTagWithName("DateTime")
	if err != nil {
		return err
	}
	data := tag.Value().Bytes()
	data = bytes.Trim(data, "\x00")

	//Mon Jan 2 15:04:05 -0700 MST 2006
	date, err := time.Parse("2006:01:02 15:04:05", string(data))
	if err != nil {
		return err
	}
	info.Date = date

	return nil
}

// getNameFromFileName extracts new name from old file name
func fillWithFileNameData(path string, info *ImageInfo) error {
	var re1 = regexp.MustCompile(`(?m)^.*([0-9]{4}[0,1][0-9][0-3][0-9]_[0-2][0-9][0-9]{4})\.(.{3})$`)
	match1 := re1.FindStringSubmatch(path)

	if len(match1) > 0 {
		date, err := time.Parse("20060102_150405", match1[1])
		if err != nil {
			return errors.Wrapf(err, "could not parse date string >%s< as YYYYMMDD_HHMMSS", match1[1])
		}
		info.Date = date
	} else {
		var re2 = regexp.MustCompile(`(?m)^.*([0-9]{4}[0,1][0-9][0-3][0-9]).*\.(.{3})$`)
		match2 := re2.FindStringSubmatch(path)

		if len(match2) > 0 {
			date, err := time.Parse("20060102", match2[1])
			if err != nil {
				return errors.Wrapf(err, "could not parse date string >%s< as YYYYMMDD", match2[1])
			}
			info.Date = date
		}
	}

	return nil
}

// getExif extracts the exif data from the file
func getExif(path string) (*exif.IfdBuilder, error) {
	parser := jpegstructure.NewJpegMediaParser()
	mediaContext, err := parser.ParseFile(path)
	if err != nil {
		return nil, errors.Wrapf(err, "could not get exif from file >%s<", path)
	}
	segmentList := mediaContext.(*jpegstructure.SegmentList)

	ifdPath := "IFD0"
	rootIb, err := segmentList.ConstructExifBuilder()
	ifdIb, err := exif.GetOrCreateIbFromRootIb(rootIb, ifdPath)
	if err != nil {
		return nil, errors.Wrapf(err, "could not get exif reader for path >%s< for file >%s<", ifdPath, path)
	}
	return ifdIb, nil
}
