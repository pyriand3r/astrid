# Astrid

Image and video file mover and renamer

I created Astrid a) for training purpose and b) because i was to lazy to search for a tool that already does what Astrid 
does.
It moves image files from an input directory to a destination directory renaming it on the fly accourding to the found 
exif information or tries to extract the proper date from the original file name. Actually it copies the file to the 
destination and removes the original after successfully creating the new file.

ATM it is all hardcoded to my needs but i plan to make everything configurable. See the ticket list for all planned 
features and todos. 

DISCLAIMER: I am not responsible for any loss of images or files you encounter using Astrid.

# Why that name?

Well, my first intentation was to name the tool "gophomov" as an abbreviation of it's purpose. But after thinking about 
it the name was to boring and uncreative. So i looked for an new name and the first thing that came to my Mind was "Astrid".
Don't ask me why, i don't know :)