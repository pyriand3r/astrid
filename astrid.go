package main

import (
	"flag"
	"fmt"
	"github.com/apex/log"
	"github.com/apex/log/handlers/text"
	"github.com/pkg/errors"
	"io"
	"os"
	"path/filepath"
	"pyriand3r/astrid/info_extractor"
	"strings"
)

// Inputs holds a slice of all configured input destinations
type Inputs []string

// String returns the string representation of the slice
func (i *Inputs) String() string {
	val := "["
	for x, entry := range *i {
		if x != 0 {
			val += ", "
		}
		val += entry
	}
	return val + "]"
}

// Set adds a new value to the slice
func (i *Inputs) Set(value string) error {
	*i = append(*i, value)
	return nil
}

// config holds the parsers configuration
type config struct {
	In  Inputs
	Out string
}

var cfg config
var debug bool
var count = 0

// main starts the process
func main() {

	flag.Var(&cfg.In, "in", "Some description for this param.")
	flag.StringVar(&cfg.Out, "out", "", "Destination root folder")
	flag.BoolVar(&debug, "debug", false, "activate debug output")
	flag.Parse()

	configureLogging(debug)

	if cfg.Out[len(cfg.Out)-1:] != "/" {
		cfg.Out += "/"
	}

	if err := checkDestWritable(cfg.Out); err != nil {
		log.WithError(err).Fatalf("out folder >%s< is not writable. Aborting", cfg.Out)
	}

	for _, in := range cfg.In {
		if err := processFolder(in); err != nil {
			log.WithError(err).Fatalf("could not process images in folder <%s>", in)
		}
	}
}

// configureLoggin configures the apex logger
func configureLogging(debug bool) {
	log.SetHandler(text.New(os.Stderr))

	if debug {
		log.SetLevel(log.DebugLevel)
		log.Info("set log level to \"debug\"")
	} else {
		log.SetLevel(log.InfoLevel)
	}
}

// checkDestWritable checks if the destination folder is writable
func checkDestWritable(out string) error {
	return nil
}

// processFolder processes a folder scanning ot for files to process
func processFolder(in string) error {
	if err := filepath.Walk(in, processFile); err != nil {
		return errors.Wrapf(err, "could not walk folder >%s<", in)
	}
	return nil
}

// processFile processes one single file
func processFile(path string, info os.FileInfo, err error) error {
	if info.IsDir() {
		return nil
	}

	imageInfo, err := info_extractor.ExtractImageInfo(path)
	if err != nil {
		return err
	}

	if err := createDest(imageInfo); err != nil {
		return err
	}

	if err := copyFile(path, imageInfo); err != nil {
		return err
	}

	if err := deleteOriginal(path); err != nil {
		return err
	}

	return nil
}

// createDest creates the destination folder
func createDest(info info_extractor.ImageInfo) error {
	dest := cfg.Out + info.Date.Format("2006/01")
	log.Debug(dest)
	if _, err := os.Stat(dest); os.IsNotExist(err) {
		if err := os.MkdirAll(dest, os.ModePerm); err != nil {
			return errors.Wrapf(err, "could not create destination path >%s<", dest)
		}
	}
	return nil
}

// copyFile copies the file to destination folder and renames it
func copyFile(original string, info info_extractor.ImageInfo) error {
	count++
	counter := fmt.Sprintf("%04d", count)
	dest := cfg.Out + info.Date.Format("2006/01/20060102_150405_") + counter + strings.ToLower(info.Ext)

	source, err := os.Open(original)
	if err != nil {
		return err
	}
	defer func() {
		_ = source.Close()
	}()

	destination, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer func() {
		_ = destination.Close()
	}()

	if _, err := io.Copy(destination, source); err != nil {
		return err
	}

	return nil
}

// deleteOriginal deletes the file und path
func deleteOriginal(path string) error {
	if err := os.Remove(path); err != nil {
		return errors.Wrapf(err, "could not delete file >%s<", path)
	}
	return nil
}
